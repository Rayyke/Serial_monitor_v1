/**********************************/
/*      Author:  Peter Cizmar     */
/**********************************/

/* Both libs are managing writing data on my display.
 */
#include <ssd1306.h>
#include <font6x8.h>

/* Buttons control scroll in manual scroll mode.
 */
#define BUTTON_UP 2
#define BUTTON_DOWN 3

/* Switch 1 is used for scroll mode: ON - auto-scroll, OFF - manual scroll. Can be changed anytime during run.
 * Switch 2 is not used. 
 * Switch 3-6 is used to choose proper baud rate for serial communication. Can be changed only before device is powered up.
 * There is total 16 combinations of 4-pin setting that provide different baud rate. 
 * 
 * 0000 -> 300
 * 0001 -> 600
 * 0010 -> 1200
 * 0011 -> 2400
 * 0100 -> 4800
 * 0101 -> 9600
 * 0110 -> 19200
 * 0111 -> 38400
 * 1000 -> 57600
 * 1001 -> 74880
 * 1010 -> 115200
 * 1011 -> 230400
 * 1100 -> 250000
 * 1101 -> 500000
 * 1110 -> 1000000
 * 1111 -> 2000000
 * 
 * Note 1: 9600 is mostly enaugh and recommended (0101)
 * Note 2: My display, together with Atmega 328p chip (5 V, 16 MHz) can safely handle around 4 FPS atm (with 9600 baud rate), 
 *         which means all chars can be replaced 4 times per second. Less baud raud was not sufficient for data transfer.
 *         Atmega 328p chip (5 V, 16 MHz) could not handle more baudrate. However, when I write only like 2
 *         chars per line, i could safely achieve 80-100 FPS. This is caused by some unefficient parts of the code. 
 *         Also I worked with the idea to have only 1000 char storage size. I would have to rewrite half of the code
 *         to achieve better performance. There will be more efficient solution for next version of this project.
 *         In conclusion, device is suitable for some slow periodical track of data.
 */
#define PCB_SWITCH_6 4
#define PCB_SWITCH_5 5
#define PCB_SWITCH_4 6
#define PCB_SWITCH_3 7
#define PCB_SWITCH_2 8
#define PCB_SWITCH_1 9

/* Yellow LED blinks everytime monitor receives valid symbol.
 * Red LED will light up, when the buffer (memory for variables) is full and cannot receive more symbols.
 */
#define LED_YELLOW 10
#define LED_RED 11

/* My display provides 4 lines, each can contains 21 symbols. 
 * Each symbol has width 6 pixels and height 8 pixels.
 * If your display has different proportions, you need to edit these 4 macros.
 *  -> DISPLAY_CHAR_WIDTH
 *  -> DISPLAY_CHAR_HEIGHT
 *  -> DISPLAY_WIDTH
 *  -> DISPLAY_HEIGHT
 * 
 * Note 1: Next 4 mentioned functions are related to certain type of display.
 *         If you have different display, you will have to edit them.
 *            -> void displayInit();
 *            -> void displayClear();
 *            -> void displayPrintChar(char c, int pos_X, int pos_Y);
 *            -> void displayPrintHugeCharMiddle(char c);
 */
#define DISPLAY_CHAR_WIDTH 6  // px
#define DISPLAY_CHAR_HEIGHT 8 // px
#define DISPLAY_WIDTH 128     // px
#define DISPLAY_HEIGHT 32     // px
#define DISPLAY_LINES ((int)(DISPLAY_HEIGHT / DISPLAY_CHAR_HEIGHT))     // 4
#define DISPLAY_LINE_LENGTH ((int)(DISPLAY_WIDTH / DISPLAY_CHAR_WIDTH)) // 21

/* Size of the buffer that stores fetched characters.
 * In future data will be stored on SD card.
 * Also code has been poorly written, so i would not recommend to add more storage.
 */
#define STORAGE_SIZE 1001 

/* To prevent repetitive push of the buttons, there is small safety delay, that allows to push certain button
 * only once per certain amount of time.
 * LED_BLINK_DURATION is handling the lenght of the yellow LED blink (which blinks when character is received). 
 * My LEDs were a little bit too shiny (with 5 mA of current flowing), so im using PWM to reduce the brightness.
 */
#define BUTTON_PRESS_FREQUENCY 333 // ms
#define LED_BLINK_DURATION 25      // ms
#define LED_PWM 50                 // (0: min, 255: max)

/* Class, that stores all storage-related global variables, including timers + contains some core functions.
 */
class MY_STORAGE{
  private:
    char storage[STORAGE_SIZE];
    long index;
    long index_X;
    long index_Y;
    long page;
    long timerButton_1;
    long timerButton_2;
    long timerLED;

  public:
    // basic setters and getters
    void setPage(int page){
      this->page = page;
    }

    int getPage(){
      return this->page;
    }

    char getStorageChar(int val){
      return this->storage[val];
    }
  
    int getIndex(){
      return this->index;
    }

    void setIndex_X(int val){
      this->index_X = val;
    }

    int getIndex_X(){
      return this->index_X;
    }

    void setIndex_Y(int val){
      this->index_Y = val;
    }

    int getIndex_Y(){
      return this->index_Y;
    }

    void setTimerButton_1(long val){
      this->timerButton_1 = val;
    }

    long getTimerButton_1(){
      return this->timerButton_1;
    }

    void setTimerButton_2(long val){
      this->timerButton_2 = val;
    }

    long getTimerButton_2(){
      return this->timerButton_2;
    }

    void setTimerLED(long val){
      this->timerLED = val;
    }

    long getTimerLED(){
      return this->timerLED;
    }

    // other basic functions
    void prevPage(){
      page--;
    }

    void nextPage(){
      page++;
    }

    bool addChar(char c){
      if (index < STORAGE_SIZE - 1){
        if(c == '\n' || (c >= 32 && c <= 126)){
          this->storage[index++] = c;
          this->storage[index] = '\0';
  
          analogWrite(LED_YELLOW, LED_PWM);
          timerLED = millis() + LED_BLINK_DURATION;
          
          return true;  
        }
        else{
          return false;
        }
      }
      else{
        analogWrite(LED_RED, LED_PWM);
        return false;
      }
    }

    // constructor
    MY_STORAGE(){
      this->index = 0;
      this->index_X = 0;
      this->index_Y = 0;
      this->page = 0;
      this->timerButton_1 = 0;
      this->timerButton_2 = 0;
      this->timerLED = 0;  
      
      for(long i = 0; i < STORAGE_SIZE; i++){
        storage[i] = '\0';
      }
    }

    // destructor
    ~MY_STORAGE(){ 
    }
};

/*** prototypes of functions ***/
void displayInit();
void displayClear();
void displayPrintChar(char c, int pos_X, int pos_Y);
void displayPrintHugeCharMiddle(char c);
void displayShowPage(int page);
void displayAddChar(char c);
int lastPage();
void checkButtons();

/*** global variables ***/
MY_STORAGE storage; // contains core globals
bool flag_1 = false; // helps with clearing my screen
bool prevSwitchPCB_1 = (digitalRead(PCB_SWITCH_1) == HIGH) ? true : false; // stores previous state of 1st switch

/* Do on chip setup.
 */
void setup() {
  char bin[5]; // stores the position of switches, used for setting baud rate

  // setting inputs
  pinMode(BUTTON_UP, INPUT);
  pinMode(BUTTON_DOWN, INPUT);
  pinMode(PCB_SWITCH_6, INPUT);
  pinMode(PCB_SWITCH_5, INPUT);
  pinMode(PCB_SWITCH_4, INPUT);
  pinMode(PCB_SWITCH_3, INPUT);
  pinMode(PCB_SWITCH_2, INPUT);
  pinMode(PCB_SWITCH_1, INPUT);

  // setting outputs
  pinMode(LED_YELLOW, OUTPUT);
  analogWrite(LED_YELLOW, LED_PWM);

  pinMode(LED_RED, OUTPUT);
  analogWrite(LED_RED, LED_PWM);

  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  /* Display init + clearing.
   * Device will initiate serial communication after 3+ seconds from the moment of powering up.
   * It is recommended to wait at least 5 seconds on the transmitting side, after powering up device (simply add delay(5000) in the beginning of a setup())
   */
  displayInit();
  displayClear();
  displayPrintHugeCharMiddle('3');
  delay(1000);
  displayPrintHugeCharMiddle('2');
  delay(1000);
  displayPrintHugeCharMiddle('1');
  delay(1000);
  displayClear();

  
  // setting baud rate, using the PCB switches
  bin[0] = (digitalRead(PCB_SWITCH_3) == HIGH) ? '1' : '0';
  bin[1] = (digitalRead(PCB_SWITCH_4) == HIGH) ? '1' : '0';
  bin[2] = (digitalRead(PCB_SWITCH_5) == HIGH) ? '1' : '0';
  bin[3] = (digitalRead(PCB_SWITCH_6) == HIGH) ? '1' : '0';
  bin[4] = '\0';

  
  // begins serial communication with certain baud rate
  if(strcmp(bin, "0000") == 0){
    Serial.begin(300);
  }
  else if(strcmp(bin, "0001") == 0){
    Serial.begin(600);  
  }
  else if(strcmp(bin, "0010") == 0){
    Serial.begin(1200);
  }
  else if(strcmp(bin, "0011") == 0){
    Serial.begin(2400);
  }
  else if(strcmp(bin, "0100") == 0){
    Serial.begin(4800);
  }
  else if(strcmp(bin, "0101") == 0){
    Serial.begin(9600);
  }
  else if(strcmp(bin, "0110") == 0){
    Serial.begin(19200);
  }
  else if(strcmp(bin, "0111") == 0){
    Serial.begin(38400);
  }
  else if(strcmp(bin, "1000") == 0){
    Serial.begin(57600);
  }
  else if(strcmp(bin, "1001") == 0){
    Serial.begin(74880);
  }
  else if(strcmp(bin, "1010") == 0){
    Serial.begin(115200);
  }
  else if(strcmp(bin, "1011") == 0){
    Serial.begin(230400);
  }
  else if(strcmp(bin, "1100") == 0){
    Serial.begin(250000);
  }
  else if(strcmp(bin, "1101") == 0){
    Serial.begin(500000);
  }
  else if(strcmp(bin, "1110") == 0){
    Serial.begin(1000000);
  }
  else{ // strcmp(bin, "1111") == 0
    Serial.begin(2000000);
  }
  
  // once device is ready, turn off red and yellow LED
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
}

/* Do in loop.
 */
void loop() {
  char c = '\0';
  
  // listening for incoming bytes/chars
  while(Serial.available() > 0){
    c = Serial.read();
    
    storage.addChar(c); // adds fetched char to storage

    // shows data on screen either in auto-scroll mode, or manual scroll mode
    if(storage.getIndex() < STORAGE_SIZE - 1){
      if(digitalRead(PCB_SWITCH_1) == LOW){
        prevSwitchPCB_1 = false;  
      }
      else{ // digitalRead(PCB_SWITCH_1) == HIGH
        storage.setPage(lastPage());
        
        if(prevSwitchPCB_1 == false){
          displayClear();
          displayShowPage(storage.getPage());  
        }
        
        prevSwitchPCB_1 = true;
      }  

      displayAddChar(c); 
    }
  } 
 
  // checks, if any button is being pressed (available only in manual scroll mode)
  if(digitalRead(PCB_SWITCH_1) == LOW){
    checkButtons();  
  }

  // turns off yellow led, if the certain amount of time passed
  if(millis() + LED_BLINK_DURATION - storage.getTimerLED() >= LED_BLINK_DURATION){
    digitalWrite(LED_YELLOW, LOW);
  }
}

/* Functions needed for setting up my display.
 */
void displayInit(){
  ssd1306_128x32_i2c_init();
  ssd1306_setFixedFont(ssd1306xled_font6x8);
}

/* Functions needed for clearing my display.
 */
void displayClear(){
  ssd1306_fillScreen(0x00);
}

/* Function prints given to char to given position.
 */
void displayPrintChar(char c, int pos_X, int pos_Y){
  char buff[2]; // function from included lib only accepts char array as input
  buff[0] = c;
  buff[1] = '\0';
  
  ssd1306_printFixed (pos_X, pos_Y, buff, STYLE_NORMAL);
}

/* Function prints one huge char (4x normal size) in the middle of the screen
 */
void displayPrintHugeCharMiddle(char c){
  char buff[2]; // function from included lib only accepts char array as input
  buff[0] = c;
  buff[1] = '\0';
  
  int pos_X = (DISPLAY_WIDTH / 2) - (2 * DISPLAY_CHAR_WIDTH);
  int pos_Y = (DISPLAY_HEIGHT / 2) - (2 * DISPLAY_CHAR_HEIGHT);

  ssd1306_printFixedN (pos_X,  pos_Y, buff, STYLE_NORMAL, FONT_SIZE_4X);
}

/* Function shows data from storage to my screen, based on selected page (page counting starts at 0).
 */
void displayShowPage(int page){
  int pageCounter = 0;
  int lineCounter = 0;
  int charCounter = 0;
  int index = 0;
  int pos_X = 0;
  int pos_Y = 0;
  char c;

  // skips pages before requested page (the unefficient part mentioned before, bigger storage = longer calculation)
  while(pageCounter < page){
    while(lineCounter < DISPLAY_LINES){
      while(charCounter < DISPLAY_LINE_LENGTH){
        if(storage.getStorageChar(index) == '\n'){
          index++;
          break;
        }
        
        charCounter++;
        index++;
      }

      charCounter = 0;
      lineCounter++;
    }

    lineCounter = 0;
    pageCounter++;
  }

  pageCounter = 0;
  lineCounter = 0;
  charCounter = 0;

  // prints requested page
  while(lineCounter < DISPLAY_LINES){
    while(charCounter < DISPLAY_LINE_LENGTH){
      if(storage.getStorageChar(index) == '\0'){
        return;
      }

      if(storage.getStorageChar(index) == '\n'){
        index++;

        if(pos_Y == DISPLAY_CHAR_HEIGHT * (DISPLAY_LINES - 1)){
          flag_1 = true;
        }
        break;
      }

      char c = storage.getStorageChar(index);

      if(flag_1 == true){
        flag_1 = false;

        if(page == lastPage() && storage.getIndex() < STORAGE_SIZE - 1){
          displayClear();  
        }
      }
      
      displayPrintChar(c, pos_X, pos_Y);

      if(pos_X == DISPLAY_CHAR_WIDTH * (DISPLAY_LINE_LENGTH - 1) && pos_Y == DISPLAY_CHAR_HEIGHT * (DISPLAY_LINES - 1)){
        flag_1 = true;
      }
      
      charCounter++;
      pos_X += DISPLAY_CHAR_WIDTH;
      index++;
    }

    charCounter = 0;
    pos_X = 0;
    lineCounter++;
    pos_Y += DISPLAY_CHAR_HEIGHT;
  }
}

/* Adds char with current index to display.
 * If it´s '\n', it will just add line. 
 */
void displayAddChar(char c){
  if(c >= 32 && c <= 126){
    if(storage.getPage() == lastPage()){
      if(storage.getIndex_X() == 0 && storage.getIndex_Y() == 0){
        displayClear();
      }
      
      displayPrintChar(c, storage.getIndex_X(), storage.getIndex_Y()); 
    }
    
    storage.setIndex_X(storage.getIndex_X() + DISPLAY_CHAR_WIDTH);
  
    if(storage.getIndex_X() >= DISPLAY_CHAR_WIDTH * DISPLAY_LINE_LENGTH){
      storage.setIndex_X(0);
      storage.setIndex_Y(storage.getIndex_Y() + DISPLAY_CHAR_HEIGHT); 
  
      if(storage.getIndex_Y() >= DISPLAY_CHAR_HEIGHT * DISPLAY_LINES){
        storage.setIndex_Y(0);
      }
    }  
  }
  else if(c == '\n'){
    if(storage.getPage() == lastPage()){
      if(storage.getIndex_X() == 0 && storage.getIndex_Y() == 0){
        displayClear();
      } 
    }
    
    storage.setIndex_X(0);
    storage.setIndex_Y(storage.getIndex_Y() + DISPLAY_CHAR_HEIGHT); 
  
    if(storage.getIndex_Y() >= DISPLAY_CHAR_HEIGHT * DISPLAY_LINES){
      storage.setIndex_Y(0);
    }  
  } 
  else{
    
  }
}

/* Function calculates and returns last page (page counting starts at 0).
 * The another unefficient part mentioned before, bigger storage = longer calculation.
 */
int lastPage(){
  int pageCounter = 0;
  int lineCounter = 0;
  int charCounter = 0;
  int index = 0;

  while(1){
    while(lineCounter < DISPLAY_LINES){
      while(charCounter < DISPLAY_LINE_LENGTH){
        if(storage.getStorageChar(index) == '\0'){
          return (lineCounter == 0 && charCounter == 0) ? pageCounter - 1 : pageCounter;
        }

        if(storage.getStorageChar(index) == '\n'){
          index++;
          break;
        }
        
        charCounter++;
        index++;
      }

      charCounter = 0;
      lineCounter++;
    }

    lineCounter = 0;
    pageCounter++;
  }
}

/* Function checks if any buttong is being pushed.
 */
void checkButtons(){
  if(digitalRead(BUTTON_UP) == HIGH && (millis() + BUTTON_PRESS_FREQUENCY - storage.getTimerButton_1()) >= BUTTON_PRESS_FREQUENCY){
    storage.setTimerButton_1(millis() + BUTTON_PRESS_FREQUENCY);

    if(storage.getPage() > 0){
      storage.prevPage();
      displayClear();
      displayShowPage(storage.getPage()); 
    }
  }

  if(digitalRead(BUTTON_DOWN) == HIGH && (millis() + BUTTON_PRESS_FREQUENCY - storage.getTimerButton_2()) >= BUTTON_PRESS_FREQUENCY){
    storage.setTimerButton_2(millis() + BUTTON_PRESS_FREQUENCY);

    if(storage.getPage() < lastPage()){
      storage.nextPage(); 
      displayClear();
      displayShowPage(storage.getPage());
    }
  }
}
